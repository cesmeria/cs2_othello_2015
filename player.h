#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <vector>
using namespace std;

class Player {
	
private:

    //initialize board
    Board * board;
    Side myside;
    Side opponentside;
    

public:

    Player(Side side);
    ~Player();
    
    //setboard
    void setboard(Board * newboard)
    {
		board = newboard;
	}

    //helper funciton to do random moves
    Move *randomMove();
    bool randomMovebool; //variable to use this
    
    //helper functions to make Heuristically guided moves
    int heuristicNum(Move *possibleMove); //returns heuristic value for given board
    Move *heuristicMove();
    bool heuristicMovebool; //variable to use this
    
    //helper function to make minmax decision tree moves
    bool minmaxMovebool;
    int minmaxHeuristic(Board *currentboard);
    std::vector<Move*> possibleMoves(Board * currentboard, Side side);
    int minmaxRecurse(Board * currentboard, int depth, Side side);
    Move *minmaxMove();
    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    
    bool abMovebool;
    int abRecurse(Board * currentboard, int depth, int alpha, int beta, Side side);
    
    Move *doMove(Move *opponentsMove, int msLeft);
};

#endif
