#include "player.h"
#include <stdlib.h>

//requisite minor change - cesmeria

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    //used to determine algorithm for move making
    randomMovebool = false;
    heuristicMovebool = false; //if using heuristic
    minmaxMovebool = false; //if using minmax WITHOUT alpha-beta pruning
    abMovebool = true;//minmax WITH alpha-beta pruning
    
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
     board = new Board();
     
     //establish side of player
     myside = side;
     if(myside == WHITE)
     {
		 std::cerr << "WHITE" << std::endl;
		 opponentside = BLACK;
	 }
	 else
	 {
		 std::cerr << "BLACK" << std::endl;
		 opponentside = WHITE;
	 }
}

/*
 * Destructor for the player.
 */
Player::~Player() {
	delete board;
}


//makes move if randomMovebool = true
Move *Player::randomMove()
{
	if(board->hasMoves(myside))
	{
		for(int i = 0; i < 8; i++)
		{
			for(int j = 0; j < 8; j++)
			{

				Move * randmove = new Move(i,j);
				if(board->checkMove(randmove, myside))
				{
					return randmove;
				}
				else 
				{
					delete randmove;
				}   
			}
		} 
	}

	return NULL;
}

//calculates test Heuristic given possible move
int Player::heuristicNum(Move *possibleMove)
{
	Board * copy_board = board->copy();
	if(copy_board->checkMove(possibleMove, myside))
	{
		copy_board->doMove(possibleMove, myside);
		return copy_board->count(myside);
	}
	else
	{
		return -300;
	}
}

//makes move if heuristicMovebool = true
Move *Player::heuristicMove()
{
    int possible_array[8][8];
	if(board->hasMoves(myside))
	{
		for(int i = 0; i < 8; i++)
		{
			for(int j = 0; j < 8; j++)
			{
				//generate number between 0 and 7 for random move x and y
				//coordinates
				
				Move * possiblemove = new Move(i,j);
				int hN = heuristicNum(possiblemove);
				//std::cerr << hN << " " << i  << " " << j << " " << std::endl;
				//corner - favorable
				if(((i == 0) && (j == 0)) || ((i == 0) && (j == 7)) || ((i == 7) && (j == 0)) || ((i == 7) && (j == 7)))
				{
					hN *= 3;
				}
				//adjacent to corner - not favorable
				if((hN > 0)&&(((i == 1) && (j == 0)) || ((i == 0) && (j == 1)) || ((i == 1) && (j == 1))) )
				{
					hN *= -3;
				}
				else if ((hN > 0)&&(((i == 0) && (j == 6)) || ((i == 1) && (j == 6)) || ((i == 1) && (j == 7))))
				{
					hN *= -3;
				}
				else if ((hN > 0)&&(((i == 6) && (j == 0)) || ((i == 6) && (j == 1)) || ((i == 7) && (j == 1))))
				{
					hN *= -3;
				}
				else if ((hN > 0)&&(((i == 7) && (j == 6)) || ((i == 6) && (j == 7)) || ((i == 6) && (j == 6))))
				{
					hN *= -3;
				}
				possible_array[i][j] = hN;
				
			}
		} 
		int max_i = 0;
		int max_j = 0;
		int max_val = -300;
		for(int i = 0; i < 8; i++)
		{
			for(int j = 0; j < 8; j++)
			{
                if(possible_array[i][j] > max_val)
                {
					max_i = i;
					max_j = j;
					max_val = possible_array[i][j];
					//std::cerr << possible_array[i][j] << " " << i  << " " << j << " " << std::endl;
			        //std::cerr << max_val << std::endl;
				}
			}
		} 
		//std::cerr << max_i  << " " << max_j << " " << std::endl;
		Move * heuristicmove = new Move(max_i, max_j);
		return heuristicmove;
	}

	return NULL;
}

//calculates board heurisitc for minmax algorithm: simple difference in 
//number of stones for each player
int Player::minmaxHeuristic(Board * currentboard)
{
	return (currentboard->count(myside) - currentboard->count(opponentside));
}

//returns all possible moves for a given side given currentboard board state
std::vector<Move*> Player::possibleMoves(Board * currentboard, Side side)
{
	std::vector<Move*> *possiblemoves = new std::vector<Move*>; //correct?
	for(int i = 0; i < 8; i++)
	{
		for(int j = 0; j < 8; j++)
		{
			Move * possiblemove = new Move(i, j);
			if(currentboard->checkMove(possiblemove, side))
			{
				possiblemoves->push_back(possiblemove);
			}
		}
	}
	return *possiblemoves;
	
}

//minmax recursive helper function
int Player::minmaxRecurse(Board * currentboard, int depth, Side side)
{
	//base case:
	//if desired depth has been reached
	if(depth == 0)
	{
		return minmaxHeuristic(currentboard);
	}
	//or if no more possible moves
	if(!currentboard->hasMoves(side))
	{
		return minmaxHeuristic(currentboard);
	}
	if(side == myside)
	{
		int returnval = -1000;
		std::vector<Move*> possiblemoves = possibleMoves(currentboard, side);
		for(std::vector<Move*>::iterator it = possiblemoves.begin(); it != possiblemoves.end(); ++it)
		{
			Board * newboard = new Board;
			newboard = currentboard->copy();
			newboard->doMove(*it, side);
			int newval = minmaxRecurse(newboard, depth - 1, opponentside);
			returnval = std::max(returnval, newval); //if oponent turn, want to maximize worse case
			delete newboard;
		}
		return returnval;
	}
	else
	{
		int returnval = 1000;
		std::vector<Move*> possiblemoves = possibleMoves(currentboard, side);
		for(std::vector<Move*>::iterator it = possiblemoves.begin(); it != possiblemoves.end(); ++it)
		{
			Board * newboard = new Board;
			newboard = currentboard->copy();
			newboard->doMove(*it, side);
			int newval = minmaxRecurse(newboard, depth - 1, myside);
			returnval = std::min(returnval, newval); //if myside turn, want to minimize best case
			delete newboard;
		}
		return returnval;
	}
}

//minmax alpha-beta pruning recursive helper function
int Player::abRecurse(Board * currentboard, int depth, int alpha, int beta, Side side)
{
	if(depth == 0)
	{
		return minmaxHeuristic(currentboard);
	}
	if(!currentboard->hasMoves(side))
	{
		return minmaxHeuristic(currentboard);
	}
	if(side == myside)
	{
		int returnval = -1000;
		std::vector<Move*> possiblemoves = possibleMoves(currentboard, side);
		for(std::vector<Move*>::iterator it = possiblemoves.begin(); it != possiblemoves.end(); ++it)
		{
			Board * newboard = new Board;
			newboard = currentboard->copy();
			newboard->doMove(*it, side);
			int newval = abRecurse(newboard, depth - 1, alpha, beta, opponentside);
			returnval = std::max(returnval, newval);
			alpha = std::max(alpha, returnval);
			delete newboard;
			if(alpha > beta) //prevents exploring unnecessary branches
			{
				break;
			}
		}
		return returnval;
	}
	else
	{
		int returnval = 1000;
		std::vector<Move*> possiblemoves = possibleMoves(currentboard, side);
		for(std::vector<Move*>::iterator it = possiblemoves.begin(); it != possiblemoves.end(); ++it)
		{
			Board * newboard = new Board;
			newboard = currentboard->copy();
			newboard->doMove(*it, side);
			int newval = abRecurse(newboard, depth - 1, alpha, beta, myside);
			returnval = std::min(returnval, newval);
			beta = std::min(beta, returnval);
			delete newboard;
			if(alpha > beta) //prevents exploring unnecessary branches
			{
				break;
			}
		}
		return returnval;
	}
}

//makes move if minmaxMovebool = true
//argument is integer depth of minmax decision tree
Move *Player::minmaxMove()
{
	if(!board->hasMoves(myside))
	{
		return NULL;
	}
	int moveint = -1000;
	std::vector<Move*> movespossible = possibleMoves(board, myside);
	Move* returnmove = new Move(movespossible[0]->getX(),movespossible[0]->getY());
	for(std::vector<Move*>::iterator it = movespossible.begin(); it != movespossible.end(); ++it)
	{
		Board * boardnew = new Board;
		boardnew = board->copy();
		boardnew->doMove(*it, myside);
		int othermoveint = 0;
		if(minmaxMovebool)
		{
			othermoveint = minmaxRecurse(boardnew, 1,opponentside); 
		}
		else if(abMovebool)
		{
			othermoveint = abRecurse(boardnew, 5, -1000, 1000, opponentside);
		}
		moveint = std::max(moveint, othermoveint);
		if(moveint == othermoveint)
		{
			returnmove = *it;
		}
		delete boardnew;
	}
	
	return returnmove;
	
}


/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
     
     //update board with opponent's move, if there is move to be had
     if(opponentsMove != NULL)
     {
          board->doMove(opponentsMove, opponentside);
     }
    
    //random move
    Move * mymove;
    if(randomMovebool)
    {
		mymove = randomMove();
	}
	else if (heuristicMovebool)
	{
		mymove = heuristicMove();
	}
	else if (minmaxMovebool || abMovebool)
	{
		mymove = minmaxMove();
	}
    if(mymove != NULL)
    {
		board->doMove(mymove, myside);
		return mymove;
	}
    return NULL;
}
