README file

I am the only member of my group. I improved my AI this week by first implementing the 
minimax algorithm recursively, which I verified with the testminimax program provided.
I then improved upon that by including alpha-beta pruning in my implementation.
I have (hopefully) improved the performance of the algorithm by going down 5 levels in
in the decision tree, which is made feasible by the alpha-beta pruning. Alpha-beta pruning is 
an improvement upon the origional minimax algorithm because it uses variables alpha and
beta to avoid exploring fruitless branches of the descision tree at each step. 
